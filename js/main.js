window.onload = function() {
	APP.init();

	var auth = readCookie("auth");
	if (!auth) {
		var user	 = window.prompt("Zabbix username:");
		var password = window.prompt("Zabbix password:");
		APP.authenticate(user, password);

	} else {
		JSONRPC.settings.setAuth(auth);
	}

	APP.authenticated(function() {
		createCookie("auth", JSONRPC.settings.getAuth())
		new JSONRPC.Request("host.get", {output: "extend"}).send(function(result) {
			var form = new GUI.Form().appendTo(new GUI.Body());

			var label_screen_name = new GUI.Label("Add Screen:").for("screen-name").appendTo(form);
			var input_screen_name = new GUI.TextInput("screen-name").appendTo(form);

			var label_rows = new GUI.Label("Rows:").for("rows").appendTo(form);
			var input_rows = new GUI.TextInput("rows").attr("value", APP.settings.default_rows).appendTo(form);
			var label_columns = new GUI.Label("Columns:").for("columns").appendTo(form);
			var input_columns = new GUI.TextInput("columns").attr("value", APP.settings.default_columns).appendTo(form);
			var button_redesign_table = new GUI.ButtonInput().attr("value", "Redesign table").event("click", function() {
				table = new GUI.Table({
					rows: input_rows.value,
					columns: input_columns.value
				});
				form.replaceChild(table, form.getElementsByTagName("table")[0]);


			}).appendTo(form);

			var options = []
			for (var i = 0; i < result.length; i++) {
				options.push([ result[i].host, result[i].hostid ]);
			}
			var select_host = new GUI.Select({
				options: [ [ "Choose a host", "_choose" ] ].concat(options)
			}).event("change", function() {
				var hostid = this.options[this.selectedIndex].value;
				if (hostid !== "_choose") {
					new JSONRPC.Request("graph.get", {output: "extend", hostids: hostid, sortfield: "name" }).send(function(result) {
						var graph_options = []
						for (var i = 0; i < result.length; i++) {
							graph_options.push([ result[i].name, result[i].graphid ]);
						}
						table.forEachRow(function(i, row) {
							table.forEachColumn(row, function(j, cell) {
								var order_specific_options = [];
								for (var k = 0; k < graph_options.length; k++) {
									order_specific_options.push([
										graph_options[k][0],
										i + 'x' + j + '_' + graph_options[k][1]
									]);
								}
								var select_graph = new GUI.Select({
									options: [ [ "Choose a graph", "_choose" ] ].concat(order_specific_options)
								}).attr("name", "screens").placeAt(cell);
							});
						});
						console.log(result);
					});
				}
			}).appendTo(form);
			var table = new GUI.Table({
				rows: APP.settings.default_rows,
				columns: APP.settings.default_columns
			}).appendTo(form);
			var button_save = new GUI.ButtonInput().attr("value", "Save").event("click", function() {
				var name = input_screen_name.value; 
				var rows = input_rows.value;
				var columns = input_columns.value;
				var screen_items = [];
				table.forEachCell(function(i, cell) {
					var regexp = /^(\d+)x(\d+)_(\d+)$/;
					var matches = regexp.exec(cell.getElementsByTagName('select')[0].value);
					if (matches !== null) {
						screen_items.push({
							screenid: 16,
							resourcetype: 0,
							resourceid: matches[3],
							width: 500,
							height: 100,
							rowspan: 0,
							colspan: 0,
							x: matches[2],
							y: matches[1],
						});
					}
				});
				new JSONRPC.Request("screen.create", {
					name: name,
					vsize: rows,
					hsize: columns,
					screenitems:
					screen_items
				}).send(function(result) {
					console.log(result);
					alert("Success!");
				});


			}).appendTo(form);
		});
	});

}
