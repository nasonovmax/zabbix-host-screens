var APP = {

}

APP.init = function() {
	JSONRPC.settings.setPath(this.settings.getPath());
}

APP.authenticate = function(user, password) {
	var request = new JSONRPC.Request(
		"user.login",
		{ 
			user: user,
			password: password,
		}
	);
	
	request.send(function(result) {
		JSONRPC.settings.setAuth(result);
	});
}

APP.authenticated = function(f) {
	var interval = setInterval(function() {
		if (JSONRPC.settings.auth) {
			clearInterval(interval);
			f();
		}
	}, 1000);
}

APP.settings = {
	path: "/zabbix/api_jsonrpc.php",
	default_rows: 12,
	default_columns: 2,
};

APP.settings.getPath = function() {
	return this.path;
}

APP.settings.setPath = function(path) {
	this.path = path;
}
