var JSONRPC = {

}

JSONRPC.Request = function(method, params) {
	this.jsonrpc		= "2.0";
	this.method			= method;
	this.params			= params;
	this.id				= Math.round(Math.random() * 100000) + 1000; 
	this.auth			= JSONRPC.settings.auth === undefined ? null : JSONRPC.settings.auth;

}
JSONRPC.Request.prototype.send = function(callback) {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200) {
			var response = new JSONRPC.Response(xmlhttp.responseText);
			if (callback !== undefined) {
				if (response.result !== undefined) {
					callback(response.result);
				} else {
					alert("Error: " + response.error.message + " " + response.error.data);
				}
			}
		}
	}
	xmlhttp.open(
		"POST"
		, JSONRPC.settings.path
		, true
	);
	xmlhttp.setRequestHeader("X-Requested-With", "XMLHttpRequest");
	xmlhttp.setRequestHeader("Content-type","application/json");
	var json_string = JSON.stringify(this);
	console.log(json_string);
	xmlhttp.send(json_string);
}

JSONRPC.Response = function(string) {
	var response = JSON.parse(string);

	this.result = response.result;
	this.error = response.error;
}

JSONRPC.settings = {
	path: "",
	auth: "",
};

JSONRPC.settings.getAuth = function() {
	return this.auth;
}

JSONRPC.settings.setAuth = function(auth) {
	this.auth = auth;
}

JSONRPC.settings.getPath = function() {
	return this.path;
}

JSONRPC.settings.setPath = function(path) {
	this.path = path;
}

